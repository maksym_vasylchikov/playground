qq_custom = {}
qq_custom.config = {}
qq_custom.addRemoveIcon = function(id) {
	if (typeof this.config.multiple == 'undefined') {
		alert('multiple mode undefined');
		return;
	}
	if (this.config.multiple) {
		var container = $('.qq-upload-list li').eq(id);
	}
	else {
		var container = $('.qq-upload-list li');
	}
	container.find('.qq-upload-finished')
		.addClass('icon-trash')
		.css('display', 'inline-block')
		.attr('title', 'Remove selected file')
//		.tooltip()
		.on('click', function() {
			$(this).parent().hide();
			$(this).siblings(":input[type=hidden]").remove();
		});
	return this;
}
qq_custom.addHiddenInput = function(id, name, value) {
	if (typeof this.config.multiple == 'undefined') {
		alert('multiple mode undefined');
		return;
	}
	if (this.config.multiple) {
		var container = $('.qq-upload-list li').eq(id);
	}
	else {
		var container = $('.qq-upload-list li');
	}
	container.prepend(
		$('<input>', {
			type: 'hidden',
			name: (this.config.multiple ? name+'[]' : name),
			value: value
		})
	);
	return this;
}