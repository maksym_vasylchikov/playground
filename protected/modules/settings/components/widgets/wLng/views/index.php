<div class="float">
	<?php $form = $this->beginWidget(
		'CActiveForm', array(
		'htmlOptions' => array(
			'class' => 'form-horizontal',
		))) ?>

		<?= $form->dropDownList($model, 'lang', Yii::app()->params['translatedLanguages'], array('class'=>'form-control js-lng-autosubmit', 'data-init'=>'chosen-list', 'data-width'=>'100px')) ?>

		<button type="submit" class="hidden js-lng-submit auto-enable ajax-post" data-href="<?= $this->controller->createUrl('wlng.save')?>" data-widget="wlng"></button>

	<?php $this->endWidget() ?>
</div>
<div class="clearfix"></div>