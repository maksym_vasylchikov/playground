<?php

class NotificationsController extends ModuleController
{
	public $guestUserModel;

	public function actions()
	{
		return array_merge(
			parent::actions(),
			array(
				'wsound.'=>array(
					'class'=>'application.modules.settings.components.widgets.wSound.wSound',
				),
			)
		);
	}

	public function init()
	{
		if (!parent::init()) return false;

		if (isset($_GET['hash'])) {
			// check the model for the hash
			if (!$this->guestUserModel = Users::model()->find('hash=:hash', array(':hash'=>$_GET['hash'])))
				throw new CHttpException('404', 'Page not found');

			// if this is hash of already loged in user, show regular member area
			if ((!Yii::app()->user->isGuest)&&(Yii::app()->user->id == $this->guestUserModel->id)) {
				$this->guestUserModel = null;
			} elseif (!Yii::app()->user->isGuest) {
				Yii::app()->user->logout(false);
				$this->redirect($_SERVER['REQUEST_URI']);
			}
		}

		return true;
	}

	public function actionInstantunsubscribe($it)
	{
		// get user model
		if ($this->guestUserModel) {
			$user = $this->guestUserModel;
		} elseif (!Yii::app()->user->isGuest) {
			if (!$user = Yii::app()->user->model)
				throw new CException('Model not found');
		} else {
			$this->redirect(Yii::app()->user->loginUrl);
		}

		$criteria = new CDbCriteria();
		$criteria->with = [
			'idBlocked'=>[
				'on'=>'idBlocked.id_user=:id_user',
				'params'=>[':id_user'=>$user->id],
			]
		];
		$criteria->addCondition('t.editable=1');
		if ($it!='all') {
			$criteria->addCondition('t.id=:id');
			$criteria->params = [':id'=>$it];
		}

		if (!$models = EmailType::model()->findAll($criteria))
			$this->redirect(['index', 'hash'=>$user->hash], true, 302, 'redirect');

		foreach ($models as $model) {
			if (!$model->idBlocked) {
				$blocked = new EmailBlocked();
				$blocked->id_user = $user->id;
				$blocked->id_type = $model->id;
				$blocked->save(false);
			}
		}

		$this->render('instantunsubscribe', array(
			'user'=>$user,
			'models'=>$models,
		));
	}

	public function actionIndex()
	{
		// get user model
		if ($this->guestUserModel) {
			$user = $this->guestUserModel;
		} elseif (!Yii::app()->user->isGuest) {
			if (!$user = Yii::app()->user->model)
				throw new CException('Model not found');
		} else {
			$this->redirect(Yii::app()->user->loginUrl);
		}

		// get email type models
		$models = EmailCategory::loadStructure($user->id);

		if (isset($_POST['id_category'])) {
			// update database
			$postIds = isset($_POST['emailTypes']) ? $_POST['emailTypes'] : array();
			foreach ($models as $category) {
				if ($category->id == $_POST['id_category'])
				foreach ($category->idType as $type) {
					// there IS a record (blocked), and there is checkbox (allowed). delete the block
					if ((isset($type->idBlocked[0]->id))&&(in_array($type->id, $postIds))) {
						// delete the record
						EmailBlocked::model()->deleteAllByAttributes(array('id_type'=>$type->id, 'id_user'=>$user->id));
					// there is NO record (not blocked) and there is no checkbox (not allowed), add a block
					} elseif ((!isset($type->idBlocked[0]->id))&&(!in_array($type->id, $postIds))) {
						$blocked = new EmailBlocked();
						$blocked->id_user = $user->id;
						$blocked->id_type = $type->id;
						$blocked->save();
					}
				}
			}

			// reload models after changes
			$models = EmailCategory::loadStructure($user->id);

			// Response
			$this->appendJsonResponse(
				array('callback'=>'appMain.showToast("Changes have been saved", "success");')
			);
		}

		$this->render('index', array('models'=>$models));
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index', 'instantunsubscribe'),
				'users'=>array('*'),
			),
			array('deny',
				'users'=>array('?'),
			),
			array('deny',
				'roles'=>array(Users::ROLE_DELETED),
			),
		);
	}

	public function getTabs()
	{
		if ($this->guestUserModel) {
			return array(
			);
		}

		if (!Yii::app()->user->isGuest) {
			return parent::getTabs();
		}

		return array();
	}
} 