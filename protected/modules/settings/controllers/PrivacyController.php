<?php
class PrivacyController extends ModuleController
{
	public function init()
	{
		if (!parent::init()) return false;
		return true;
	}

	public function filters()
	{
		return CMap::mergeArray(parent::filters(), array(
		));
	}

	public function actionIndex()
	{
		$model = Yii::app()->user->model->profileBuyer;
		$model->setScenario('privacy');

		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			if (!$model->validate()) {
				$this->appendJsonResponse([
					'error'=>MyUtils::getFirstError($model),
				]);
			} else {
				$model->save(false);
				$this->appendJsonResponse([
					'callback'=>'appMain.showToast("Privacy settings have been updated", "success")',
				]);
			}

			$this->jsonResponse(['soft_redirect'=>'']);
		}

		$this->render('index', ['model'=>$model]);

	}
}