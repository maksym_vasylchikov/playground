<?php
class LinksExist extends CValidator
{
	protected function validateAttribute($object,$attribute)
	{
		//$object->$attribute
		if (strpos($object->$attribute, "http://") !== false || strpos($object->$attribute, "https://") !== false) {
			$message=$this->message!==null?$this->message:Yii::t('app', 'Links are not allowed in {attribute}');
			$this->addError($object,$attribute,$message);
		}
	}
}